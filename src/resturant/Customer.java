package resturant;

import java.io.Serializable;
import java.util.ArrayList;

public class Customer implements Serializable {
	private static int count=0;
	private static int indexOfArray=0;
	private String name;
	private String family;
	private String phoneNumber;
	private int accountCredit;
//	private int scoreForChef;
	private Chef chef;
	private ArrayList<String> chooseFoods;
	private ArrayList<Integer> priceOfFoods;
	private String nameOfChef;
	private String familyOfChef;
	private Bill bill ;
	
	
	
	//////////////
//	private ArrayList<Integer> pricesOfFoods;
//	private ArrayList<String> nameOfFoods;
//	/////////////
//	public ArrayList<Integer> getPricesOfFoods() {
//		return pricesOfFoods;
//	}
//	public void setPricesOfFoods(ArrayList<Integer> pricesOfFoods) {
//		this.pricesOfFoods = pricesOfFoods;
//	}
//	public ArrayList<String> getNameOfFoods() {
//		return nameOfFoods;
//	}
//	public void setNameOfFoods(ArrayList<String> nameOfFoods) {
//		this.nameOfFoods = nameOfFoods;
//	}
//	public int totalCalculate() {
//		int result = 0 ;
//		for(Integer priceOfFood : this.pricesOfFoods ) {
//			result += priceOfFood;
//		}
//		return result;
//	}
//	public void showBill() {
//		for(int i = 0 ; i < nameOfFoods.size() ; i++) {
//			System.out.println(nameOfFoods.get(i) + " : " + pricesOfFoods.get(i));
//		}
//		int total = totalCalculate();
//		System.out.println("Total : " + total);
//	}
//	public void addFoodToNameOfFood(String food) {
//		nameOfFoods.add(food);
//	}
//	public void addPriceToPricesOfFood(int price) {
//		pricesOfFoods.add(price);
//	}





////////////////////////////////////
	
	
	
	
	
	public Customer(String name, String family, String phoneNumber) {
		super();
		count++;
		indexOfArray = count - 1;
		this.name = name;
		this.family = family;
		this.phoneNumber = phoneNumber;
	}
	public Customer(String name, String family, String phoneNumber, int accountCredit) {
		super();
		count++;
		indexOfArray = count - 1;
		this.name = name;
		this.family = family;
		this.phoneNumber = phoneNumber;
		this.accountCredit = accountCredit;
	}
	
//	public static void countIndexOfArray() {
//		indexOfArray++;
//	}
	public static int getIndexOfArray() {
		return indexOfArray;
	}
	public static void setIndexOfArrayForDecrease() {
		count--;
		indexOfArray--;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getAccountCredit() {
		return accountCredit;
	}

	public void setAccountCredit(int accountCredit) {
		this.accountCredit = accountCredit;
	}

	/*
	 * public int getScoreForChef() { return scoreForChef; } // TODO clean it public
	 * void setScoreForChef(int scoreForChef) { while (true) { if (scoreForChef > 10
	 * && scoreForChef < 0) {
	 * System.out.println("You should enter phoneNumber 0 to 10!"); } else {
	 * this.scoreForChef = scoreForChef; break; } } }
	 */
	
	public void giveTheScorToChef(int score) {
		chef.setScore(score);
	}

	public Chef getChef() {
		
		return chef;
	}

	public void setChef(Chef chef) {
		this.chef = chef;
	}

	public ArrayList<String> getChooseFoods() {
		return chooseFoods;
	}

	public void setChooseFoods(ArrayList<String> chooseFoods) {
		this.chooseFoods = chooseFoods;
	}
	public void increaseAccountCredit(int accountCredit) {
		this.accountCredit += accountCredit;
	}
	public ArrayList<Integer> getPriceOfFoods() {
		return priceOfFoods;
	}
	public void setPriceOfFoods(ArrayList<Integer> priceOfFoods) {
		this.priceOfFoods = priceOfFoods;
	}
//	public int calculateTotal() {
//		int result = 0 ;
//		for(Integer priceOfFood : this.priceOfFoods ) {
//			result += priceOfFood;
//		}
//		return result;
//	}
	public Bill getBill() {
		return bill;
	}
	public void setBill(Bill bill) {
		this.bill = bill;
	}
	public void reduceAccountCredit(int price) {
		this.accountCredit -= price;
		
	}

}
