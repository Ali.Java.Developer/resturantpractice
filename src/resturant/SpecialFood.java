package resturant;

import java.io.Serializable;
import java.util.ArrayList;

public class SpecialFood implements Serializable {
	private static int count=0;
	private static int indexOfArray=0;
	
	private ArrayList<String> specialFoods = new ArrayList<String>();
	private ArrayList<Integer> priceOfFoods = new ArrayList<Integer>();

	public SpecialFood(ArrayList<String> specialFoods , ArrayList<Integer> priceOfFoods) {
		this.specialFoods = specialFoods;
		this.priceOfFoods = priceOfFoods;
		count++;
		indexOfArray = count - 1;
	}

	public ArrayList<String> getSpecialFoods() {
		return specialFoods;
	}

	public void setSpecialFoods(ArrayList<String> specialFoods) {
		this.specialFoods = specialFoods;
	}
//	public static void countIndexOfArray() {
//		indexOfArray++;
//	}
	public static int getIndexOfArray() {
		return indexOfArray;
	}
	public ArrayList<Integer> getPriceOfFoods() {
		return priceOfFoods;
	}

	public void setPriceOfFoods(ArrayList<Integer> priceOfFoods) {
		this.priceOfFoods = priceOfFoods;
	}

	public static void setIndexOfArrayForDecrease() {
		count--;
		indexOfArray--;
	}

}
