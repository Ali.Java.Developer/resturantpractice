package resturant;

import java.util.ArrayList;

public class Bill {
	private ArrayList<Integer> pricesOfFoods = new ArrayList<Integer>();
	private ArrayList<String> nameOfFoods = new ArrayList<String>();
	
	public ArrayList<Integer> getPricesOfFoods() {
		return pricesOfFoods;
	}
	public void setPricesOfFoods(ArrayList<Integer> pricesOfFoods) {
		this.pricesOfFoods = pricesOfFoods;
	}
	public ArrayList<String> getNameOfFoods() {
		return nameOfFoods;
	}
	public void setNameOfFoods(ArrayList<String> nameOfFoods) {
		this.nameOfFoods = nameOfFoods;
	}
	public int totalCalculate() {
		int result = 0 ;
		for(Integer priceOfFood : this.pricesOfFoods ) {
			result += priceOfFood;
		}
		return result;
	}
	public void showBill() {
		for(int i = 0 ; i < nameOfFoods.size() ; i++) {
			System.out.println(nameOfFoods.get(i) + " : " + pricesOfFoods.get(i));
		}
		int total = totalCalculate();
		System.out.println("Total : " + total);
	}
	public void addFoodToNameOfFood(String food) {
		nameOfFoods.add(food);
	}
	public void addPriceToPricesOfFood(int price) {
		pricesOfFoods.add(price);
	}
	
}
