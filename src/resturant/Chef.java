package resturant;

import java.io.Serializable;
import java.util.ArrayList;

public class Chef implements Serializable{
	private static int count=0;
	private static int indexOfArray=0;
	private String name;
	private String family;
	private int score;
	private SpecialFood specialFood;
	
	public Chef(String name, String family, SpecialFood specialFood) {
		super();
		count++;
		indexOfArray = count - 1;
		this.name = name;
		this.family = family;
		this.specialFood = specialFood;
	}
	
//	public static void countIndexOfArray() {
//		indexOfArray++;
//	}
	public static int getIndexOfArray() {
		return indexOfArray;
	}
	public static void setIndexOfArrayForDecrease() {
		count--;
		indexOfArray--;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	/**
	 * This method return Class of SpecialFood
	 * @return
	 */
	public SpecialFood getSpecialFood() {
		return specialFood;
	}
	public void setSpecialFood(SpecialFood specialFood) {
		this.specialFood = specialFood;
	}
	/**
	 * This method return ArrayList of string of specialFood variables
	 * @return
	 */
	public ArrayList<String> getSpecialMenu(){
		return specialFood.getSpecialFoods();
	}
	public void changeScore(int Score) {
		this.score = (this.score + score) / 2;
	}
	

}
