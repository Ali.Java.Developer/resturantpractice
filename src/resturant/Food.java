package resturant;

import java.io.Serializable;

public class Food implements Serializable {
	String[] name;
	String[] price;
	String[] time;
	public Food(String[] name, String[] price) {
		super();
		this.name = name;
		this.price = price;
	}
	public Food(String[] name, String[] price, String[] time) {
		super();
		this.name = name;
		this.price = price;
		this.time = time;
	}
	public String[] getName() {
		return name;
	}
	public void setName(String[] name) {
		this.name = name;
	}
	public String[] getPrice() {
		return price;
	}
	public void setPrice(String[] price) {
		this.price = price;
	}
	public String[] getTime() {
		return time;
	}
	public void setTime(String[] time) {
		this.time = time;
	}
	
	
}
